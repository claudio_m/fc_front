import '@/app/ui/global.css';

export const metadata = {
  title: 'FC Front',
  description: 'A front-end for a face recognition app',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  )
}
