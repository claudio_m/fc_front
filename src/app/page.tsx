import UserLogo from '@/app/components/user_logo'
import Link from 'next/link'

export default function index() {
        return <div>
                <h1 className="m-3">Home Page</h1>
                <div className='flex items-center justify-around'>
                        <UserLogo/>
                        <div className='grid col-span'>
                                <Link href="/register" className='bg-green-500 hover:bg-white text-white font-bold py-2 px-4 rounded-full m-2'>Cadastar</Link>
                                <Link href="/login" className='bg-green-500 hover:bg-white text-white font-bold py-2 px-4 rounded-full m-2'>Login</Link>
                        </div>
                </div>
        </div>
}