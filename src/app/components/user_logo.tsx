import Image from "next/image"

export default function UserLogo() {
        return (
                <div className="flex items-center m-4">
                        <Image 
                                src="/img/user_img.png" 
                                width={320}
                                height={300}
                                alt="IMG"/>
                </div>
        ); 
}
